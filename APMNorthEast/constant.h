//
//  constant.h
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#ifndef constant_h
#define constant_h
#define UIAppDelegate \
((AppDelegate*)[[UIApplication sharedApplication]delegate])
//constants for Webservice
//demo_tellsid
//tellsid
//http://tellsid.softintelligence.co.uk/
//http://apmnortheast.softintelligence.co.uk
//http://apmnortheast.softintelligence.co.uk/Eventsapi/events_list

/*  LIVE URLS
 
 #define kBaseURL @"http://live.thechangeconsultancy.co/tellsid/index.php/Api/"
#define kImageURL @"http://live.thechangeconsultancy.co/tellsid/assets/upload/apmnortheast/"
#define kChatImageURL @"http://live.thechangeconsultancy.co/tellsid/assets/upload/apmnortheast/chatimg/"

#define kEventsBaseURL @"http://live.thechangeconsultancy.co/apmnortheast/"
#define kEventImageURL @"http://live.thechangeconsultancy.co/apmnortheast/"*/

//#define kBaseURL @"http://live.softintelligence.co.uk/tellsid/index.php/Api/"
//#define kImageURL @"http://live.softintelligence.co.uk/tellsid/assets/upload/apmnortheast/"
//#define kChatImageURL @"http://live.softintelligence.co.uk/tellsid/assets/upload/apmnortheast/chatimg/"
//
//#define kEventsBaseURL @"http://live.softintelligence.co.uk/apmnortheast/"
//#define kEventImageURL @"http://live.softintelligence.co.uk/apmnortheast/"



//#define kBaseURL @"http://demo.thechangeconsultancy.co/tellsid/Api/"
//#define kImageURL @"http://demo.thechangeconsultancy.co/tellsid/assets/upload/apmnortheast/"
//#define kChatImageURL @"http://demo.thechangeconsultancy.co/tellsid/assets/upload/apmnortheast/chatimg/"
//
//#define kEventsBaseURL @"http://demo.thechangeconsultancy.co/apmnortheast/"
//#define kEventImageURL @"http://demo.thechangeconsultancy.co/apmnortheast/"


//#define kBaseURL @"http://demo.thechangeconsultancy.co/demo_tellsid/index.php/Api/"
//#define kImageURL @"http://demo.thechangeconsultancy.co/demo_tellsid/assets/upload/apmnortheast/"
//#define kChatImageURL @"http://demo.thechangeconsultancy.co/demo_tellsid/assets/upload/apmnortheast/chatimg/"


//#define kEventsBaseURL @"http://demo.thechangeconsultancy.co/apmnortheast-demo/"
//#define kEventImageURL @"http://demo.thechangeconsultancy.co/apmnortheast-demo/"

//#define kBaseURL @"http://tellsid.softintelligence.co.uk/index.php/Api/"
//#define kImageURL @"http://tellsid.softintelligence.co.uk/assets/upload/apmnortheast/"
//#define kChatImageURL @"http://tellsid.softintelligence.co.uk/assets/upload/apmnortheast/chatimg/"
//
//#define kEventsBaseURL @"http://apmnortheast.softintelligence.co.uk/"
//#define kEventImageURL @"http://apmnortheast.softintelligence.co.uk/"

//#define kBaseURL @"http://demo.thechangeconsultancy.co/tellsid/Api/"
//#define kImageURL @"http://demo.thechangeconsultancy.co/tellsid/assets/upload/apmnortheast/"
//#define kChatImageURL @"http://demo.thechangeconsultancy.co/tellsid/assets/upload/apmnortheast/chatimg/"
//
//#define kEventsBaseURL @"http://demo.thechangeconsultancy.co/apmnortheast/"
//#define kEventImageURL @"http://demo.thechangeconsultancy.co/apmnortheast/"

//Demo APM
//#define kBaseURL @"https://demo.thechangeconsultancy.co/tellsid/Api/"
//#define kImageURL @"https://demo.thechangeconsultancy.co/tellsid/assets/upload/apmnortheast/"//@"https://demo.thechangeconsultancy.co/tellsid/Api/assets/upload/apmnortheast/"
//#define kChatImageURL @"https://demo.thechangeconsultancy.co/tellsid/Api/assets/upload/apmnortheast/chatimg/"
//
//#define kEventsBaseURL @"https://demo.thechangeconsultancy.co/apmnortheast/"
//#define kEventImageURL @"https://demo.thechangeconsultancy.co/apmnortheast/"

//Correct Demo
#define kBaseURL @"https://demo.thechangeconsultancy.co/tellsid/Api/"
#define kImageURL @"https://demo.thechangeconsultancy.co/tellsid/assets/upload/businesscentral/"
#define kChatImageURL @"https://demo.thechangeconsultancy.co/tellsid/Api/assets/upload/businesscentral/chatimg/"

#define kEventsBaseURL @"https://demo.thechangeconsultancy.co/businesscentral/"
#define kEventImageURL @"https://demo.thechangeconsultancy.co/businesscentral/"

//Correct Live
//#define kBaseURL @"https://tellsid.softintelligence.co.uk/index.php/Api/"
//#define kImageURL @"https://tellsid.softintelligence.co.uk/assets/upload/apmnortheast/"
//#define kChatImageURL @"https://tellsid.softintelligence.co.uk/assets/upload/apmnortheast/chatimg/"
//
//#define kEventsBaseURL @"http://live.softintelligence.co.uk/apmnortheast/"
//#define kEventImageURL @"http://live.softintelligence.co.uk/apmnortheast/"

#define kAppNameAPI @"BusinessCentral"//@"APMNorthEast"
//#define kAppSecret @"stationmaster@1"
#define kAppNameAlert @"Business Central"
#define kInternetOff @"The Internet connection appears to be offline."
#define khudColour [UIColor colorWithRed:248/255.0f green:141/255.0f blue:14/255.0f alpha:1]
// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#endif /* constant_h */
