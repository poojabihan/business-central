//
//  DetailViewController.h
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DetailViewController : UIViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *imageVw;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottonView;
@property (weak, nonatomic) IBOutlet UILabel *dateAndTime;

@property (weak, nonatomic) IBOutlet UITextView *detailText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContrsint;

@property NSString *date, *details, *imageStr,*locationStr;


@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property NSString * imageName;

@end
