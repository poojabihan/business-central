//
//  History+CoreDataProperties.m
//  APMNorthEast
//
//  Created by Tarun Sharma on 16/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "History+CoreDataProperties.h"

@implementation History (CoreDataProperties)

+ (NSFetchRequest<History *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"History"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic image;
@dynamic locationdetail;
@dynamic msgdetail;

@end
