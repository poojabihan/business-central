//
//  CandidateRegistrationVC.m
//  APMNorthEast
//
//  Created by Alok Mishra on 25/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//
#define kAppNameAlert @"Business Central"
#define kAppNameAPI @"BusinessCentral"
//#define kBaseURL @"http://live.thechangeconsultancy.co/apmnortheast/index.php/Api/"


#import "CandidateRegistrationVC.h"
#import <MBProgressHUD.h>
#import "Reachability.h"
#import "Webservice.h"
#import <SCLAlertView.h>
#import "constant.h"
#import "AppDelegate.h"
#import "QuartzCore/QuartzCore.h"

#define MAX_HEIGHT 100
@import Firebase;
@interface CandidateRegistrationVC ()
{
    MBProgressHUD *hud, * pleaseWaitHud,*registeringHUD;
    NSArray *questionDataArray;
    SCLAlertView *customAlert;
    UITextView * textViewInAlert;
    NSArray * dictArray,* inboxDictArray, * itemChatArray,*resultArray;
    NSString * inboxStr;
}
@property NSString * stringToCheckVC,*emailURL,*itemPostURL,* URLString,* refreshedToken;

@end

@implementation CandidateRegistrationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    [self shadowToView:_view1];
    [self shadowToView:_view2];
    [self shadowToView:_view3];
    [self shadowToView:_view4];
    [self shadowToView:_view5];
    [self shadowToView:_viewBottom];
    
    _feedback_btn.backgroundColor=[UIColor clearColor];
    _feedback_pop_btn.backgroundColor=[UIColor clearColor];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    // getting an Email String
    NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
    
    NSString * isRegistered=[standardUserDefaults stringForKey:@"registered"];
    
    NSLog(@"email id in view did load %@ and is registered %@",emailIdString,isRegistered);
    if (emailIdString==NULL && isRegistered==NULL)
    {
        registeringHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
        // registeringHUD.contentColor =khudColour;
        registeringHUD.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
        //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
        // Set the label text.
        registeringHUD.label.text = NSLocalizedString(@"Registering PushNotification...", @"HUD loading title");
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    }
    else if(emailIdString==NULL&&[isRegistered isEqualToString:@"yes"]){
        [self presentingEmailPrompt];
    }
    // Do any additional setup after loading the view.*/
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    if (screenRect.size.height == 568)
    {
        // this is an iPhone 5+
        [_tblView setScrollEnabled:YES];
    }
    else {
        [_tblView setScrollEnabled:NO];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //New
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
}

- (void) shadowToView:(UIView *)currentView
{
    
    if (currentView == _viewBottom){
        currentView.layer.masksToBounds = NO;
        currentView.layer.cornerRadius = 0.0;
    }
    else{
        
        currentView.layer.masksToBounds = NO;
        currentView.layer.cornerRadius = 10.0;
    }
    
    currentView.layer.shadowRadius  = 10.0f;
    currentView.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
    currentView.layer.shadowOffset  = CGSizeMake(0.0f, 3.0f); //CGSizeMake(0.0f, 0.0f);
    currentView.layer.shadowOpacity = 0.3f;
    currentView.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath1      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(currentView.bounds, shadowInsets)];
    
    currentView.layer.shadowPath    = shadowPath1.CGPath;
    
        
    /*currentView.layer.masksToBounds = NO;
    currentView.layer.cornerRadius = 20.0;
    
    currentView.layer.shadowColor = [UIColor blackColor].CGColor;
    currentView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    currentView.layer.shadowOpacity = 0.6f;
    currentView.layer.shadowRadius = 20.0;
    currentView.layer.shadowPath = shadowPath.CGPath;*/
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if ([customAlert isVisible]) {
        [customAlert hideView];
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    NSLog(@"View DID APPEAR Called");
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [hud hideAnimated:YES];
    });
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
        [self getQuessionarieMethod];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [hud hideAnimated:YES];
        });
    });
    NSLog(@"string name before empty %@",self.stringToCheckVC);
    if ([self.stringToCheckVC isEqualToString:@"Messages"])
    {
        //[NSThread detachNewThreadSelector:@selector(historyLoad) toTarget:self withObject:nil];
        self.stringToCheckVC=@"";
        
    }
    else if ([self.stringToCheckVC isEqualToString:@"History"])
    {
        //[NSThread detachNewThreadSelector:@selector(inboxLoad) toTarget:self withObject:nil];
        [self performSelectorOnMainThread:@selector(inboxLoad) withObject:nil waitUntilDone:YES];
        
        
        self.stringToCheckVC=@"";
        
    }
    else if ([self.stringToCheckVC isEqualToString:@"Chat"])
    {
        //[NSThread detachNewThreadSelector:@selector(inboxLoad) toTarget:self withObject:nil];
        [self performSelectorOnMainThread:@selector(inboxLoad) withObject:nil waitUntilDone:YES];
        self.stringToCheckVC=@"";
        
    }
    NSLog(@"string name after empty %@",self.stringToCheckVC);
    
}
-(void)inboxLoad{
   
    NSLog(@"INdox Load");
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSLog(@"ID called in ViewDIDLOad=>%@",[notification object]);
    NSString * instanceID = [NSString stringWithFormat:@"%@",[notification object]];
    NSLog(@"String ViewDIDLOad%@",instanceID);
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:@"yes" forKey:@"registered"];
    [standardUserDefaults synchronize];
    dispatch_async(dispatch_get_main_queue(), ^{
        [registeringHUD hideAnimated:YES];
    });
    [self presentingEmailPrompt];
}
-(void)presentingEmailPrompt
{
    
//    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:kAppNameAlert message: @"Signup or Login if you already have account."preferredStyle:UIAlertControllerStyleAlert];

    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:kAppNameAlert message: @"Please enter your email address in order to send feedback." preferredStyle:UIAlertControllerStyleAlert];

    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter your E-mail";
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [textField setKeyboardType:UIKeyboardTypeEmailAddress];
        //textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    
//    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.placeholder = @"Enter your Password";
//        textField.textColor = [UIColor blackColor];
//        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        [textField setKeyboardType:UIKeyboardTypeEmailAddress];
//        textField.secureTextEntry = YES;
//        //textField.borderStyle = UITextBorderStyleRoundedRect;
//    }];
    
    
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        pleaseWaitHud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
        //pleaseWaitHud.contentColor =khudColour;
        pleaseWaitHud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
        //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
        
        // Set the label text.
        pleaseWaitHud.label.text = NSLocalizedString(@"Please wait...", @"HUD loading title");
        NSArray * textfields = alertController.textFields;
        UITextField * Email = textfields[0];
//        UITextField * password = textfields[1];
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if(internetStatus != NotReachable)
        {
            
            
            if ([self validateEmailWithString:Email.text]) {
                self.refreshedToken=[[FIRInstanceID instanceID]token];
                NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
                
                
                NSLog(@"FCMTokenString %@",self.refreshedToken);
                
                
                //NSString * post=[NSString stringWithFormat:@"email_id_to=%@&fcm_token=%@&app_name=%@&device_id=%@&ssecrete=%@",Email.text,refreshedToken,kAppNameAPI,[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"tellsid@1"];
                
                NSDictionary *parametersDictionary = @{@"email_id_to":Email.text,
                                                       @"fcm_token":self.refreshedToken?:@"",
                                                       @"app_name":kAppNameAPI,
                                                       @"device_id":[[[UIDevice currentDevice] identifierForVendor] UUIDString]//,
//                                                       @"password":password.text
                                                       };
                
                
                self.emailURL=[NSString stringWithFormat:@"%@signup/",kBaseURL];
                
                
                [Webservice requestPostUrl:self.emailURL parameters:parametersDictionary success:^(NSDictionary *response) {
                    NSLog(@"Response : %@",response);
                    
                    if ([[response objectForKey:@"response"]isEqualToString:@"Succ"]) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [pleaseWaitHud hideAnimated:YES];
                            [standardUserDefaults setObject:[response objectForKey:@"email"] forKey:@"emailId"];
                            [standardUserDefaults synchronize];
                            
                            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                            //hud.contentColor =khudColour;
                            
                            // Set the custom view mode to show any view.
                            hud.mode = MBProgressHUDModeCustomView;
                            // Set an image view with a checkmark.
                            UIImage *image = [[UIImage imageNamed:@"CheckMark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                            hud.customView = [[UIImageView alloc] initWithImage:image];
                            // Looks a bit nicer if we make it square.
                            hud.square = YES;
                            // Optional label text.
                            hud.label.text = NSLocalizedString(@"Done", @"HUD done title");
                            
                            [hud hideAnimated:YES afterDelay:1.5f];
                            
//                            [self performSegueWithIdentifier:@"toFeedback" sender:nil];
                        });
                    }
                    else if (response==NULL) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [pleaseWaitHud hideAnimated:YES];
                        });
                        
                        [standardUserDefaults removeObjectForKey:@"emailId"];
                        [[NSUserDefaults standardUserDefaults]synchronize ];
                        
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Something went wrong!" message:@"Please try again later" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                        }];
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                        NSLog(@"response is null");
                        
                    }
                    else{
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [pleaseWaitHud hideAnimated:YES];
                        });
                        
                        [standardUserDefaults removeObjectForKey:@"emailId"];
                        [[NSUserDefaults standardUserDefaults]synchronize ];
                        
//                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Something went wrong!" message:@"Please try again later" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning!" message:[NSString stringWithFormat:@"%@",[response objectForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                        }];
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                    }
                    
                } failure:^(NSError *error) {
                    NSLog(@"Error: %@", error);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        /// NSUserDefaults * removeUD = [NSUserDefaults standardUserDefaults];
                        [pleaseWaitHud hideAnimated:YES];
                        [standardUserDefaults removeObjectForKey:@"emailId"];
                        [[NSUserDefaults standardUserDefaults]synchronize ];
                        
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Something went wrong!" message:@"Please try again later" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                        }];
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                        NSLog(@"response is null");
                    });
                    
                    
                }];
                
                
                
            }else if ([Email.text isEqualToString:@""]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [pleaseWaitHud hideAnimated:YES];
                });
                UIAlertController *alertError = [UIAlertController  alertControllerWithTitle:kAppNameAlert  message:@"Email Address is Required"  preferredStyle:UIAlertControllerStyleAlert];
                
                [alertError addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                       {
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                           
                                           [self presentViewController:alertController animated:YES completion:nil];
                                           
                                       }]];
                
                [self presentViewController:alertError animated:YES completion:nil];
            }
//            else if ([password.text isEqualToString:@""]){
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [pleaseWaitHud hideAnimated:YES];
//                });
//                UIAlertController *alertError = [UIAlertController  alertControllerWithTitle:kAppNameAlert  message:@"Password is Required"  preferredStyle:UIAlertControllerStyleAlert];
//
//                [alertError addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//                                       {
//                                           [self dismissViewControllerAnimated:YES completion:nil];
//
//                                           [self presentViewController:alertController animated:YES completion:nil];
//
//                                       }]];
//
//                [self presentViewController:alertError animated:YES completion:nil];
//            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [pleaseWaitHud hideAnimated:YES];
                });
                UIAlertController *alertError = [UIAlertController  alertControllerWithTitle:kAppNameAlert  message:@"Please enter valid email address."  preferredStyle:UIAlertControllerStyleAlert];
                
                [alertError addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                       {
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                           
                                           [self presentViewController:alertController animated:YES completion:nil];
                                           
                                       }]];
                
                [self presentViewController:alertError animated:YES completion:nil];
                
                
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [pleaseWaitHud hideAnimated:YES];
            });
            UIAlertController *alertError = [UIAlertController  alertControllerWithTitle:kAppNameAlert  message:@"The Internet connection appears to be offline."  preferredStyle:UIAlertControllerStyleAlert];
            
            [alertError addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                       
                                       [self presentViewController:alertController animated:YES completion:nil];
                                       
                                   }]];
            
            [self presentViewController:alertError animated:YES completion:nil];
            
            
        }
        
    }]];
    
//    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
    
    
}
-(void)getQuessionarieMethod{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        questionDataArray=[[NSArray alloc]init];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        if (!(emailIdString==NULL)) {
            NSString * strForQues;
            
            NSLog(@"email id in Appdelgate %@",emailIdString);
            if (emailIdString==NULL) {
                
                // strForQues = [NSString stringWithFormat:@"%@badgecount?email_id=",kBaseURL];
                strForQues = [NSString stringWithFormat:@"getquestion?email_id=&app_name=%@",kAppNameAPI];
            }
            else{
                
                
                strForQues = [NSString stringWithFormat:@"getquestion?email_id=%@&app_name=%@",emailIdString,kAppNameAPI];
            }
            
            
            
            NSURL *baseURL = [NSURL URLWithString:kBaseURL];
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
            
            [manager GET:strForQues parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                NSLog(@"response = %@", responseObject);
                dispatch_async(dispatch_get_main_queue(), ^{
                    questionDataArray = [responseObject objectForKey:@"response"];
                    NSLog(@"Response in bool %d ",UIAppDelegate._isQuesFromNotify);
                    if ([[[responseObject objectForKey:@"total"]stringValue] isEqualToString:@"1"]) {
                        UIAppDelegate._isQuesFromNotify=NO;
                        [self showQuestionarrieAlert:[[questionDataArray objectAtIndex:0]objectForKey:@"question"]];
                        
                    }else if (UIAppDelegate._isQuesFromNotify==YES &&[[[responseObject objectForKey:@"total"]stringValue] isEqualToString:@"0"])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAppDelegate._isQuesFromNotify=NO;
                            SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                            SCLButton *doneButton = [alert addButton:@"Done" actionBlock:^{
                                
                            }];
                            
                            doneButton.buttonFormatBlock = ^NSDictionary* (void)
                            {
                                NSMutableDictionary *buttonConfig = [[NSMutableDictionary alloc] init];
                                
                                buttonConfig[@"backgroundColor"] = khudColour;
                                buttonConfig[@"textColor"] = [UIColor whiteColor];
                                //buttonConfig[@"borderWidth"] = @2.0f;
                                //buttonConfig[@"borderColor"] = [UIColor greenColor];
                                
                                return buttonConfig;
                            };
                            alert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/ReceivedMessage.caf", [NSBundle mainBundle].resourcePath]];
                            [alert showCustom:self image:[UIImage imageNamed:@"IconQuessionaire.png"] color:[UIColor whiteColor] title:kAppNameAlert subTitle:@"You have already submitted your Feedback." closeButtonTitle:nil duration:0.0f];
                        });
                        
                        
                        
                        
                    }
                    
                });
                
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                NSLog(@"error = %@", error);
                
            }];
            
            
            
            
            
            
        }
        
    }
    
    
}

- (IBAction)btnFeedbackAction:(id)sender {
//    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
//    // getting an Email String
//    NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
//
//    NSString * isRegistered=[standardUserDefaults stringForKey:@"registered"];
//
//    NSLog(@"email id in view did load %@ and is registered %@",emailIdString,isRegistered);
//    if (emailIdString==NULL && isRegistered==NULL)
//    {
//        registeringHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
//        // registeringHUD.contentColor =khudColour;
//        registeringHUD.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
//        //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
//        // Set the label text.
//        registeringHUD.label.text = NSLocalizedString(@"Registering PushNotification...", @"HUD loading title");
//
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
//
//    }
//    else if(emailIdString==NULL&&[isRegistered isEqualToString:@"yes"]){
//        [self presentingEmailPrompt];
//    }
//    else {
        [self performSegueWithIdentifier:@"toFeedback" sender:nil];
//    }
}

#pragma mark - question Alert

-(void)showQuestionarrieAlert:(NSString *)question{
    customAlert = [[SCLAlertView alloc] init];
    textViewInAlert=[[UITextView alloc] init];
    //NSString * str1=[[questionDataArray objectAtIndex:0]objectForKey:@"question"];
    question = [question stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    CGRect textRect = [question boundingRectWithSize:CGSizeMake(200, MAX_HEIGHT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
    
    CGSize size1 = textRect.size;
    
    [textViewInAlert setFrame:CGRectMake(10, 0, 200, size1.height + 10)];
    textViewInAlert.editable = NO;
    textViewInAlert.selectable = NO;
    textViewInAlert.textAlignment=NSTextAlignmentCenter;
    [textViewInAlert setFont:[UIFont systemFontOfSize:14]];
    
    
    
    
    //textViewInAlert.text=@"ok";
    textViewInAlert.text=question;
    
    [customAlert addCustomView:textViewInAlert];
    
    //UIColor *color = khudColour;
    
    UIView *greenView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 200, 70)];
    //SCLButton *button1 = [alert addButton:@"" target:self selector:@selector(firstButton)];
    UIButton * button1=[UIButton buttonWithType:UIButtonTypeSystem];
    [button1 setBackgroundImage:[UIImage imageNamed:@"smiley.png"] forState:UIControlStateNormal];
    button1.frame=CGRectMake(20, 10, 50, 50);
    [greenView addSubview:button1];
    //SCLButton *button2 = [alert addButton:@"" target:self selector:@selector(firstButton)];
    UIButton * button2=[UIButton buttonWithType:UIButtonTypeSystem];
    [button2 setBackgroundImage:[UIImage imageNamed:@"Neutral.png"] forState:UIControlStateNormal];
    button2.frame=CGRectMake(80, 10, 50, 50);
    [greenView addSubview:button2];
    
    //SCLButton *button3 = [alert addButton:@"" target:self selector:@selector(firstButton)];
    UIButton * button3=[UIButton buttonWithType:UIButtonTypeSystem];
    [button3 setBackgroundImage:[UIImage imageNamed:@"sad.png"] forState:UIControlStateNormal];
    button3.frame=CGRectMake(140, 10, 50, 50);
    [greenView addSubview:button3];
    
    [customAlert addCustomView:greenView];
    
    
    [button1 addTarget:self action:@selector(smileButton) forControlEvents:UIControlEventTouchUpInside];
    [button2 addTarget:self action:@selector(neutralButton) forControlEvents:UIControlEventTouchUpInside];
    [button3 addTarget:self action:@selector(sadButton) forControlEvents:UIControlEventTouchUpInside];
    
    SCLButton *button = [customAlert addButton:@"Prefer Not To Answer" target:self selector:@selector(preferNotToAnswerButton)];
    
    button.buttonFormatBlock = ^NSDictionary* (void)
    {
        NSMutableDictionary *buttonConfig = [[NSMutableDictionary alloc] init];
        
        buttonConfig[@"backgroundColor"] = khudColour;
        buttonConfig[@"textColor"] = [UIColor whiteColor];
        //buttonConfig[@"borderWidth"] = @2.0f;
        //buttonConfig[@"borderColor"] = [UIColor greenColor];
        
        return buttonConfig;
    };
    
    NSDate *currentYear=[[NSDate alloc]init];
    currentYear=[NSDate date];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy"];
    NSString *currentYearString = [formatter1 stringFromDate:currentYear];
    NSLog(@"Current year is %@",currentYearString);
    
    customAlert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/ReceivedMessage.caf", [NSBundle mainBundle].resourcePath]];
    [customAlert showCustom:self image:[UIImage imageNamed:@"IconQuessionaire.png"] color:[UIColor whiteColor] title:kAppNameAlert subTitle:[NSString stringWithFormat:@"%@ © Soft Intelligence Data Centre.",currentYearString] closeButtonTitle:nil duration:0.0f];
    
    // @"2016 © Soft Intelligence Data Centre."
}

- (void)smileButton
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //hud.contentColor =khudColour;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        //[customAlert hideView];
        [self submitSmileyMethod:@"Happy"];
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
        
        
        
        
        
        
    }
    
    
    NSLog(@"SmileButton button tapped");
}
- (void)neutralButton
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    // hud.contentColor =khudColour;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        //[customAlert hideView];
        [self submitSmileyMethod:@"Neutral"];
        
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
        
        
        
        
    }
    
    
    NSLog(@"NeutralButton button tapped");
}
- (void)sadButton
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    // hud.contentColor =khudColour;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        //[customAlert hideView];
        [self submitSmileyMethod:@"Sad"];
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
        
        
        
    }
    NSLog(@"SadButton button tapped");
    
}


- (void)preferNotToAnswerButton
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //hud.contentColor =khudColour;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        //[customAlert hideView];
        [self submitSmileyMethod:@"Prefer Not To Answer"];
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
        
        
        
        
    }
    
    NSLog(@"NotToAnswerButton  tapped");
    
}
-(void)submitSmileyMethod:(NSString *)answer{
    
    NSString *emailIdString;
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    emailIdString = [standardUserDefaults stringForKey:@"emailId"];
    NSLog(@"email id %@",emailIdString);
    
    //NSString *post = [NSString stringWithFormat:@"email_id=%@&ques_id=%@&reply=%@",emailIdString,[[questionDataArray objectAtIndex:0] objectForKey:@"ques_id"],answer];
    
    
    self.URLString=[NSString stringWithFormat:@"%@getuseranser/",kBaseURL];
    
    
    //NSURL *baseURL = [NSURL URLWithString:self.URLString];
    
    
    
    
    
    NSDictionary *dictParam = @{@"email_id":emailIdString,@"ques_id":[[questionDataArray objectAtIndex:0] objectForKey:@"ques_id"],@"reply":answer,@"app_name":kAppNameAPI};
    [Webservice requestPostUrl:self.URLString parameters:dictParam success:^(NSDictionary *response) {
        NSLog(@"Response : %@",response);
        
        if ([response objectForKey:@"response"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
                [customAlert hideView];
                
                
                SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                SCLButton *doneButton = [alert addButton:@"Done" actionBlock:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self getQuessionarieMethod];
                    });
                }];
                
                doneButton.buttonFormatBlock = ^NSDictionary* (void)
                {
                    NSMutableDictionary *buttonConfig = [[NSMutableDictionary alloc] init];
                    
                    buttonConfig[@"backgroundColor"] = khudColour;
                    buttonConfig[@"textColor"] = [UIColor whiteColor];
                    //buttonConfig[@"borderWidth"] = @2.0f;
                    //buttonConfig[@"borderColor"] = [UIColor greenColor];
                    
                    return buttonConfig;
                };
                
                
                ///[alert showSuccess:kAppNameAlert subTitle:@"Thanks for your Feedback." closeButtonTitle:nil duration:0.0f];
                [alert showCustom:[UIImage imageNamed:@"IconQuessionaire.png"] color:[UIColor whiteColor] title:kAppNameAlert subTitle:@"Thanks for your Feedback." closeButtonTitle:nil duration:0.0f];
                
            });
        }
        else if (response==NULL) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
            [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
            
            
            NSLog(@"response is null");
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
            [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
            
        }
        
        
    } failure:^(NSError *error) {
        NSLog(@"Error: %@", error);
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
        
        
        NSLog(@"response is null");
        
        
    }];
    
    
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)feedback_btn_click:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"All feedback is processed by Soft Intelligence, All information remains anonymous to the APM North East Branch, So Please be as honest as you can" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
    });
}

-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

@end
