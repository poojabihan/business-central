//
//  EventListTableVC.h
//  APMNorthEast
//
//  Created by Tarun Sharma on 18/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventListTableVC : UIViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopConstraint;
@property NSMutableArray * eventBookingArray;
@property NSString * getCurrentEventsURL,*eventMainImageURL;
@property (weak, nonatomic) IBOutlet UITableView *tblEvent;
@end
