//
//  Chat+CoreDataProperties.h
//  APMNorthEast
//
//  Created by Tarun Sharma on 16/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "Chat+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Chat (CoreDataProperties)

+ (NSFetchRequest<Chat *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *changeitid;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, copy) NSString *posttype;
@property (nullable, nonatomic, copy) NSString *usertype;

@end

NS_ASSUME_NONNULL_END
