//
//  Messages+CoreDataClass.h
//  APMNorthEast
//
//  Created by Tarun Sharma on 16/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Messages : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Messages+CoreDataProperties.h"
