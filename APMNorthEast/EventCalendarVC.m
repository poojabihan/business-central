//
//  EventCalendarVC.m
//  APMNorthEast
//
//  Created by Tarun Sharma on 17/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "EventCalendarVC.h"
#import "MBProgressHUD.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "constant.h"
#import "Webservice.h"
#import "EventCell.h"
#import "EventDetailsViewController.h"
#import "UIImageView+WebCache.h"

@interface EventCalendarVC ()
{
    
    MBProgressHUD * hud;
    BOOL searchEnabled;
    void * _KVOContext;
    
    
}
@property (strong, nonatomic) NSDateFormatter *dateFormatter1;
@property (strong, nonatomic) UIPanGestureRecognizer *scopeGesture;
@end

@implementation EventCalendarVC
#pragma mark - Life Cycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
        self.dateFormatter1 = [[NSDateFormatter alloc] init];
        self.dateFormatter1.dateFormat = @"yyyy-MM-dd";
        //self.datesArray=[[NSMutableArray alloc]init];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([self hasTopNotch]) {
        _logoTopConstraint.constant = 40.0;
    }
    else {
        _logoTopConstraint.constant = 25.0;
    }

    //New
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.estimatedRowHeight = 145;
    if ([[UIDevice currentDevice].model hasPrefix:@"iPad"]) {
        self.eventCaledarHeightConstraint.constant = 400;
    }
    [self.eventCalendar selectDate:[NSDate date] scrollToDate:YES];
    [self.eventCalendar deselectDate:[NSDate date]];
    [self.eventCalendar.appearance setTodayColor:[UIColor colorWithHex:@"#a14b00"]];
    
    // [self.bookingCalendar selectDate:[NSDate date] scrollToDate:YES];
    self.eventCalendar.placeholderType = FSCalendarPlaceholderTypeNone;
    
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self.eventCalendar action:@selector(handleScopeGesture:)];
    panGesture.delegate = self;
    panGesture.minimumNumberOfTouches = 1;
    panGesture.maximumNumberOfTouches = 2;
    [self.view addGestureRecognizer:panGesture];
    self.scopeGesture = panGesture;
    
    // While the scope gesture begin, the pan gesture of tableView should cancel.
    [self.tableView.panGestureRecognizer requireGestureRecognizerToFail:panGesture];
    
    [self.eventCalendar addObserver:self forKeyPath:@"scope" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:_KVOContext];
    
    self.eventCalendar.scope = FSCalendarScopeMonth;
    
    // For UITest
    self.eventCalendar.accessibilityIdentifier = @"calendar";
    
    // [self.navigationItem setTitle:@"New Booking"];
//    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -80) forBarMetrics:UIBarMetricsDefault];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 20, 20);
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];

  //  [self setTwoLineTitle:@"Calendar" color:[UIColor colorWithHex:@"#FFFFFF"] font:[UIFont boldSystemFontOfSize: 24.0f]];
    NSLog(@"Present Month %@",self.eventCalendar.currentPage);
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kReachabilityChangedNotification object:nil];
    [self loadingElementsInEvents];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) goBack:(UIButton *) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark  Reachability Delegates

-(void) triggerAction:(NSNotification *) notification
{
    NSLog (@"Notification Data %@",notification.userInfo);
    if ([[notification name] isEqualToString:@"kNetworkReachabilityChangedNotification"])
    {
        NSLog (@"Successfully received the kNetworkReachabilityChangedNotification!");
        [self loadingElementsInEvents];
        //[self returnDate:self.bookingCalendar.currentPage];
        
        
    }
    
}
- (void)dealloc
{
    [self.eventCalendar removeObserver:self forKeyPath:@"scope" context:_KVOContext];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"%s",__FUNCTION__);
}
- (void)setTwoLineTitle:(NSString *)titleText color:(UIColor *)color font:(UIFont *)font {
    CGFloat titleLabelWidth = [UIScreen mainScreen].bounds.size.width/2;
    
    UIView *wrapperView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = font;
    label.adjustsFontSizeToFitWidth = YES;
    label.textAlignment = UIBaselineAdjustmentAlignCenters;
    label.textColor = color;
    label.text = titleText;
    [wrapperView addSubview:label];
    
    
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshClick:)];
    self.navigationItem.rightBarButtonItem=refreshButton;
    self.navigationItem.titleView = wrapperView;
}
- (IBAction)refreshClick:(id)sender
{
    // do something or handle Search Button Action.
    searchEnabled = NO;
    self.dateString=@"";
   // [self.datesArray removeAllObjects];
    for (NSDate *date in self.eventCalendar.selectedDates) {
        [self.eventCalendar deselectDate:date];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.eventCalendar reloadData];
        [self.tableView reloadData];
    });
    
}
-(void)loadingElementsInEvents{
    
    self.eventBookingArray=[[NSMutableArray alloc]init];
    self.subtitleDict=[[NSMutableDictionary alloc]init];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        // Do any additional setup after loading the view.
        
        
        
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
       
        
        // Set the label text.
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        
        
       
        
        
        self.getCurrentEventsURL=[NSString stringWithFormat:@"%@Eventsapi/events_list/",kEventsBaseURL];
        
        [Webservice requestPostUrl:self.getCurrentEventsURL parameters:nil success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if ([[response objectForKey:@"response"] isEqualToString:@"Succ"]) {
                self.eventBookingArray=[NSMutableArray arrayWithArray:[response objectForKey:@"record"]];
                self.eventSearchResults = [self.eventBookingArray mutableCopy];
                for (int i=0; i<[self.eventBookingArray count]; i++) {
                    
                    [self.subtitleDict setObject:[[self.eventBookingArray objectAtIndex:i]objectForKey:@"id"] forKey:[[self.eventBookingArray objectAtIndex:i]objectForKey:@"date"]];
                }
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    [self.eventCalendar reloadData];
                    [self.tableView reloadData];
                    
                });
                
                
            }
            
            else if (response==NULL || [[[response objectForKey:@"error"]objectForKey:@"error"]isEqualToString:@"Something is problem!."]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.eventCalendar reloadData];
                    [self.tableView reloadData];
                    
                    [hud hideAnimated:YES];
                });
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                
                
                
                NSLog(@"response is null");
                
                
            }
            else if ([[response objectForKey:@"response"] isEqualToString:@"Fail"] && [[[response objectForKey:@"error"] objectForKey:@"error"] isEqualToString:@"Record not found."]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.eventCalendar reloadData];
                    [self.tableView reloadData];
                    
                    [hud hideAnimated:YES];
                });
                
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                
                
            }
            
            
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.eventCalendar reloadData];
                [self.tableView reloadData];
                
                [hud hideAnimated:YES];
            });
            NSLog(@"Error %@",error);
        }];
        
        
        
        
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.eventCalendar reloadData];
            [self.tableView reloadData];
            
        });
        
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
    
}

- (NSString *)changeDate:(NSString *)strDate {
    NSString *dateString = strDate;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [format dateFromString:dateString];
    [format setDateFormat:@"dd,MMM yyyy"];
    NSString* finalDateString = [format stringFromDate:date];
    return finalDateString;
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (context == _KVOContext) {
        FSCalendarScope oldScope = [change[NSKeyValueChangeOldKey] unsignedIntegerValue];
        FSCalendarScope newScope = [change[NSKeyValueChangeNewKey] unsignedIntegerValue];
        NSLog(@"From %@ to %@",(oldScope==FSCalendarScopeWeek?@"week":@"month"),(newScope==FSCalendarScopeWeek?@"week":@"month"));
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - <UIGestureRecognizerDelegate>

// Whether scope gesture should begin
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    BOOL shouldBegin = self.tableView.contentOffset.y <= -self.tableView.contentInset.top;
    if (shouldBegin) {
        CGPoint velocity = [self.scopeGesture velocityInView:self.view];
        switch (self.eventCalendar.scope) {
            case FSCalendarScopeMonth:
                return velocity.y < 0;
            case FSCalendarScopeWeek:
                return velocity.y > 0;
        }
    }
    return shouldBegin;
}

#pragma mark  Alert Method

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}


- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    //    NSLog(@"%@",(calendar.scope==FSCalendarScopeWeek?@"week":@"month"));
    self.eventCaledarHeightConstraint.constant = CGRectGetHeight(bounds);
    [self.view layoutIfNeeded];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (searchEnabled) {
        return [self.eventSearchResults count];
    }
    else{
        return [self.eventBookingArray count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EventCell * eventCell= [tableView dequeueReusableCellWithIdentifier:@"EventCell" forIndexPath:indexPath];
    
    
    if (eventCell == nil) {
        eventCell = [[EventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EventCell"];
    }
    
    eventCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (searchEnabled) {
        
        eventCell.eventTitleLabel.text=[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"title"];
        eventCell.dateLabel.text=[self changeDate:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"date"]];
        
        
        
        
        eventCell.locationLabel.text=[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"venue"];
        
        eventCell.organizerLabel.text=[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"organiser"];
        
    }
    else
    {
      
        eventCell.eventTitleLabel.text=[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"title"];
        
        eventCell.dateLabel.text=[self changeDate:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"date"]];
        
        eventCell.locationLabel.text=[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"venue"];
        
        eventCell.organizerLabel.text=[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"organiser"];
        
    }
    
    NSMutableArray * imagesArray = [[NSMutableArray alloc] initWithArray:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"imagearray"]];
    
    self.eventMainImageURL= [[NSString stringWithFormat:@"%@%@",kEventImageURL, [imagesArray objectAtIndex:0]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:self.eventMainImageURL].absoluteString];
    if(image == nil)
    {
        
        [eventCell.imgEvent sd_setImageWithURL:[NSURL URLWithString:self.eventMainImageURL] placeholderImage:[UIImage imageNamed:@"ImgNotFound.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error == nil) {
                [eventCell.imgEvent setImage:image];
                
            } else {
                NSLog(@"Image downloading error: %@", [error localizedDescription]);
                [eventCell.imgEvent setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                
            }
        }];
    } else {
        [eventCell.imgEvent setImage:image];
        
    }
    return eventCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (!([self.candidateNameString length]==0) || !([[self.candidateNameString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0)){
//        return 238;
//    }
//    else {
//        return 195;
//    }
     return UITableViewAutomaticDimension;
}
- (void)updateFilteredContentForAirlineName:(NSString *)airlineName
{
    
    if (airlineName == nil) {
        
        // If empty the search results are the same as the original data
        self.eventSearchResults = [self.eventBookingArray mutableCopy];
    } else {
        
        //NSMutableArray *searchResults = [[NSMutableArray alloc] init];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(date contains[cd] %@)", airlineName];
        NSArray * search = [self.eventBookingArray filteredArrayUsingPredicate:pred];
        NSLog(@"Search Array %@",search);
        self.eventSearchResults = [NSMutableArray arrayWithArray:search];
        [self.tableView reloadData];
        
    }
    
}

#pragma mark - FSCalendarDataSource

- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    
    NSString *dateString = [self.dateFormatter1 stringFromDate:date];
    if ([self.subtitleDict.allKeys containsObject:dateString]) {
        
        return 1;
    }
    
    
    return 0;
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    
    NSLog(@"did select date %@",[self.dateFormatter1 stringFromDate:date]);
    
    self.dateString = [self.dateFormatter1 stringFromDate:date];
    if ([self.subtitleDict.allKeys containsObject:self.dateString]) {
        
        
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:self.dateString];
    }
    else{
        
        searchEnabled = NO;
        [self.tableView reloadData];
    }
    
//    if (!(date < self.bookingCalendar.today)){
//        [self.datesArray addObject:[self.dateFormatter1 stringFromDate:date]];
//    }
//      NSLog(@"Array Print Selected %@",self.datesArray);
    
}


- (void)calendar:(FSCalendar *)calendar didDeselectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
//    [self.datesArray removeObject:[self.dateFormatter1 stringFromDate:date]];
    [calendar deselectDate:date];
   // NSLog(@"Array Print DeSelected %@",self.datesArray);
    
    
}


- (void)calendarCurrentMonthDidChange:(FSCalendar *)calendar
{
    //NSLog(@"Present Month %@",self.bookingCalendar.currentPage);
    
  //  [self returnDate:self.bookingCalendar.currentPage];
    // Do something
}
- (void)calendar:(FSCalendar *)calendar willDisplayCell:(FSCalendarCell *)cell forDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
    if (monthPosition == FSCalendarMonthPositionCurrent) {
        //  NSLog(@"Date %@",[self.dateFormatter1 stringFromDate:date]);
    }
}
- (BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
//    if (date < self.evn.today && ![self.subtitleDict.allKeys containsObject:[self.dateFormatter1 stringFromDate:date]]) {
//        return NO;
//    }
//    if (date<[self.dateFormatter1 dateFromString:self.firstDateOfMonth]) {
//        return NO;
//    }
//    if (date>[self.dateFormatter1 dateFromString:self.lastDateOfMonth]) {
//        return NO;
//    }
    return YES;
    
}

#pragma mark - FSCalendarDelegateAppearance

- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date{
    if (date<self.eventCalendar.today) {
        return [UIColor lightGrayColor];
    }
//    if (date < self.bookingCalendar.today && ![self.subtitleDict.allKeys containsObject:[self.dateFormatter1 stringFromDate:date]]) {
//        return [UIColor lightGrayColor];
//    }
//    if (date<[self.dateFormatter1 dateFromString:self.firstDateOfMonth]) {
//        return [UIColor lightGrayColor];
//    }
//    if (date>[self.dateFormatter1 dateFromString:self.lastDateOfMonth]) {
//        return [UIColor lightGrayColor];
//    }
    return [UIColor whiteColor];
}

- (nullable NSArray<UIColor *> *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventDefaultColorsForDate:(NSDate *)date{
    return @[[UIColor whiteColor]];
}
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date{
    
    NSString *dateString = [self.dateFormatter1 stringFromDate:date];
    if ([self.subtitleDict.allKeys containsObject:dateString]) {
        
        return [UIColor colorWithHex:@"#01CA1F"];
    }
    
    return nil;
}

- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date{
    if (date < self.eventCalendar.today){
        return [UIColor colorWithHex:@"#ca0101"];
    }
    return nil;
    
}
// -(UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance borderDefaultColorForDate:(NSDate *)date
//{
//    NSString * key = [_calendar stringFromDate:date format:@"yyyy/MM/dd"];
//    if ([_borderDefaultColors.allKeys containsObject:key]) {
//        return _borderDefaultColors[key];
//    }
//    return appearance.borderDefaultColor = [UIColor redColor];
//}
//- (NSString *)calendar:(FSCalendar *)calendar subtitleForDate:(NSDate *)date
//{
//    NSString *dateString = [self.dateFormatter1 stringFromDate:date];
//
//    if ([self.subtitleDict.allKeys containsObject:dateString]) {
//
//        if ([self.subtitleDict[dateString] isEqualToString:@"1"]) {
//            return @"AM";
//        }
//        else if ([self.subtitleDict[dateString] isEqualToString:@"2"]){
//            return @"PM";
//        }
//        else if ([self.subtitleDict[dateString] isEqualToString:@"3"]){
//            return @"A";
//        }
//        return self.subtitleDict[dateString];
//    }
//    return nil;
//}

- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance subtitleDefaultColorForDate:(NSDate *)date{
    return [UIColor whiteColor];
}
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"eventDetailVCFromCalendar"]) {
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        
        EventDetailsViewController * eventDetailVC = [segue destinationViewController];
        if(searchEnabled){
            [eventDetailVC setEventTitleString:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"title"]];
            [eventDetailVC setSubtitleString:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"sub_title"]];
            [eventDetailVC setDateString:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"date"]];
            [eventDetailVC setTimeString:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"time"]];
            [eventDetailVC setVenueString:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"venue"]];
            [eventDetailVC setCpdString:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"cpd"]];
             [eventDetailVC setCostString:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"cost"]];
            [eventDetailVC setOrganizerString:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"organiser"]];
            [eventDetailVC setSummaryString:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"summary_text"]];
             [eventDetailVC setImagesArray:[[self.eventSearchResults objectAtIndex:indexPath.row]objectForKey:@"imagearray"]];
            
            eventDetailVC.BookUrl=[[self.eventSearchResults objectAtIndex:indexPath.row] objectForKey:@"url"];
        
        }
        else{
            [eventDetailVC setEventTitleString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"title"]];
            [eventDetailVC setSubtitleString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"sub_title"]];
            [eventDetailVC setDateString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"date"]];
            [eventDetailVC setTimeString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"time"]];
            [eventDetailVC setVenueString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"venue"]];
            [eventDetailVC setCpdString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"cpd"]];
            [eventDetailVC setCostString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"cost"]];
            [eventDetailVC setOrganizerString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"organiser"]];
            [eventDetailVC setSummaryString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"summary_text"]];
            [eventDetailVC setImagesArray:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"imagearray"]];
            eventDetailVC.BookUrl=[[self.eventBookingArray objectAtIndex:indexPath.row] objectForKey:@"url"];
            
        }
    }
}

- (BOOL)hasTopNotch {
    if (@available(iOS 11.0, *)) {
        return [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.top > 20.0;
    }
    
    return  NO;
}

@end
