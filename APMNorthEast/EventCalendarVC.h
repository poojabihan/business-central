//
//  EventCalendarVC.h
//  APMNorthEast
//
//  Created by Tarun Sharma on 17/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"

@interface EventCalendarVC : UIViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopConstraint;
@property (strong, nonatomic) IBOutlet FSCalendar *eventCalendar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *eventCaledarHeightConstraint;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray * eventBookingArray,*eventSearchResults;
@property NSMutableDictionary * subtitleDict;
@property NSString * getCurrentEventsURL,*dateString,*candidateNameString, *eventMainImageURL;

@end
