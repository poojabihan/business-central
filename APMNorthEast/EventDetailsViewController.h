//
//  EventDetailsViewController.h
//  APMNorthEast
//
//  Created by Tarun Sharma on 17/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailsViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate>
{
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIScrollView *imagesScrollView;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopConstraint;
//@property (strong, nonatomic) IBOutlet UIScrollView *parentScrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintSummaryLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLoweViewHeight;

@property (strong, nonatomic) IBOutlet UIView *upperView;
@property (strong, nonatomic) IBOutlet UIView *lowerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *upperViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lowerViewHeightConstraint;
- (IBAction)changePage:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *eventTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTitleLabel;

@property (strong, nonatomic) IBOutlet UIView *dateInfoView;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *venueLabel;
@property (strong, nonatomic) IBOutlet UILabel *cpdLabel;
@property (strong, nonatomic) IBOutlet UILabel *costLabel;
@property (strong, nonatomic) IBOutlet UILabel *organizerNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *summaryTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *Url_lbl_text;
@property (weak, nonatomic) IBOutlet UITableView *tblEvent;

@property (weak, nonatomic) IBOutlet UIButton *UrlbtnClick;
- (IBAction)ClickUrl:(id)sender;

@property(nonatomic,strong)NSString *BookUrl;
@property NSString * eventTitleString, *subtitleString,*dateString,*timeString,*venueString,*cpdString,*costString,*organizerString,*summaryString,*imageURL;
@property NSMutableArray * imagesArray;
@end
